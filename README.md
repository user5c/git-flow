# Script para crear un comando que sincroniza una rama diferente a `master` con `develop`
Ej. Si stamos trabajando en la rama `feature/foo` y hay cambios en develop que queremos en esta rama, este scrip lo que hace es hacer pasarse a la rama `develop`, descargar los cambios, volver a la rama `feature/foo` y hacer merge de `develop`.

## Configuración de comando
En la configuración se pregunta si se va usar el comando desde una terminal con Bash o [Zsh](https://www.asanzdiego.com/2018/04/instalar-y-configurar-zsh-y-ohmyzsh-en-ubuntu.html)

```
git clone https://gitlab.com/user5c/git-flow.git
cd git-flow
bash git-flow-update
```

## Uso
En cualquier repositorio en el que quieran sincronizar la rama en la que estan trabajando con `develop`, funciona el comando
```
git-flow-update
```
